import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import './index.css';
import { Header, PrivateRoute } from './_components';
import { NotFound, Comision, Comisiones, Login } from './Pages';
import * as serviceWorker from './serviceWorker';


const routing = (
	<Router>
		<div>
			<Header/>
			<Switch>
				<Route exact path="/Comisiones" component={Comisiones} />
				<PrivateRoute exact path="/ComisionesPrivada" component={Comisiones} />
				<Route exact path="/Comisiones/:id" component={Comision} />
				<Route exact path="/" component={Login} />
				<Route component={NotFound} />
			</Switch>
		</div>
	</Router>
)


ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
