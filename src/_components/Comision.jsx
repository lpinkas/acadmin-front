import React from 'react'
import { Link } from "react-router-dom";

export const Comision = ({ comision }) => {
  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">{comision.descMateria}</h5>
        <h6 className="card-subtitle mb-2 text-muted">{comision.codigo}</h6>
        <p className="card-text">{comision.nombreDocente} {comision.apellidoDocente}</p>
        <Link to={"/Comisiones/" + comision.id}>Detalle</Link>
      </div>
    </div>
  )
};