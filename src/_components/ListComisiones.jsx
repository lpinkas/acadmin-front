import React from 'react'
import { Comision } from '../_components'

export const ListComisiones = ({ comisiones }) => {
  return (
    <div>
      <center><h1>Comisiones Activas</h1></center>
      {comisiones.map((comision, index) => (
        <Comision key={index} comision={comision} />
      ))}
    </div>
  )
};