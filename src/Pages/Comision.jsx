import React from 'react';
import { comisionesService } from '../_services';

export class Comision extends React.Component {

  state = {
    comision: {
      anio: '',
      apellidoDocente: '',
      nombreDocente: '',
      codigo: '',
      codigoMateria: '',
      descMateria: '',
    }
  }

  async componentDidMount() {
    const data = await comisionesService.getById(this.props.match.params.id);
    this.setState({ comision: data });
  }

  render() {
    const { params } = this.props.match
    return (
      <div>
        <div className="bg-white">
          <h1>Detalle Comision</h1>
          <h2>{this.state.comision.anio}</h2>
          <h3>Cod Comision: {this.state.comision.codigo} <br></br> Cod Materia: {this.state.comision.codigoMateria}</h3>
          <h6>{this.state.comision.nombreDocente} {this.state.comision.apellidoDocente}</h6>
          <p>{params.id}</p>
        </div>
      </div>
    );
  }
}