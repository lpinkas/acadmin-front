import React from 'react'
import { ListComisiones } from '../_components'
import { comisionesService } from '../_services';
 
export class Comisiones extends React.Component {

  state = {
    comisiones: []
  }

  async componentDidMount() {
    const data = await comisionesService.getAll();
    this.setState({ comisiones: data.resultados });
  }

  render() {
    return (
      <div>
        <h1>Comisiones</h1>
        <ListComisiones comisiones={this.state.comisiones} />
      </div>
    );
  }
}