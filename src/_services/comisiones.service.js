import { config } from './consts';

export const comisionesService = {
    getAll,
    getById
};

async function getAll () {
    return fetch(`${config.apiUrl}/Comisiones/Activas`, { method: 'GET'} ).then(res => res.json());
}

async function getById(id){
    return fetch(`${config.apiUrl}/Comisiones/` + id, { method: 'GET' }).then(res => res.json());
}